/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibonacci;

import java.util.ArrayList;

/**
 *
 * @author pavel
 */
public class Fibonacci {

    public ArrayList<Integer> list;
    
    /**
     * setting first two parameters of fib-sequence to default values
     * 
     */
    public void init(){
        list=new ArrayList<Integer>();
        list.add(0);
        list.add(1);
    }
    
    /**
     * setting first two parameters of fib-sequence to users values
     * @param y1 first element of fib-sequence
     * @param y2 second element of fib-sequence
     */
    public void init(int y1, int y2){
        list=new ArrayList<Integer>();
        list.add(y1);
        list.add(y2);
    }
    
    /**
     * set next element of fib-sequence and return it
     * @return int next element of fib-sequence
     */
    public int next(){
        if(list.isEmpty()){
            return -1;
        }
        int last=list.size()-1;
        int last2=last-1;
        list.add(list.get(last)+list.get(last2));
        return list.get(list.size()-1);
    }
    
    /**
     * return last element od fib-sequence
     * @return last element of fib-sequence
     */

    public int current(){
        if(list.isEmpty()){
            return -1;
        }
        return list.get(list.size()-1);
    }
    
    /**
     * return index of last element
     * @return index of last element
     */
    public int currentID(){
        if(list.isEmpty()){
            return -1;
        }
        return list.lastIndexOf(list.get(list.size()-1));
    }
    
    /**
     * reset ArrayList of fib-sequence
     */
    public void reset(){
        list=new ArrayList<Integer>();
    }
    
    /**
     * get number of index in fib-sequence
     * @param index index of element in fib-sequence
     * @return int last element of fib-sequence
     */
    public int number(int index){
        int ret = -1;
        if(list.isEmpty()){
            return -1;
        }
        if(index==0){
            return list.get(0);
        }
        else if(index==1){
            return list.get(1);
        }
        else{
            
            int idx=1;
            while(idx<index){
                int t=list.get(idx)+list.get(idx-1);
                idx=idx+1;
                list.add(t);
            }
            
            int x;
            x=list.get(list.size()-1);
            return x;
        }
    }
    
}
