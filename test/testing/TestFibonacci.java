/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import fibonacci.Fibonacci;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author pavel
 */
public class TestFibonacci {
    private static int ocekavanyVysledek;
    private static Fibonacci fib;
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    @BeforeClass
    public static void setUpClass() throws Exception {
        ocekavanyVysledek=8;
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        fib=new Fibonacci();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
        fib.reset();
    }
    
    @Test
    public void test1(){
        fib.init();
        fib.number(5);
        
        for(int i=0; i<fib.list.size(); i++){
            System.out.println(fib.list.get(i));
        }
        
      	assertEquals(fib.number(0),0);
      	assertEquals(fib.number(1),1);
      	assertEquals(fib.number(2),1);
      	assertEquals(fib.number(3),2);
      	assertEquals(fib.number(4),3);
      	assertEquals(fib.number(5),5);
        assertEquals(fib.next(), ocekavanyVysledek);
        assertEquals(fib.current(), 8);
    }
    
    @Test
    public void test2(){
        fib.init();
        assertEquals(fib.current(), 1);
        for(int i=0; i<6; i++){
            fib.next();
        }
        
        assertEquals(fib.current(), 13);
    }
    
    @Test
    public void test3(){
        fib.init();
        for(int i=0; i<3; i++){
            fib.next();
        }         
            
            assertEquals(fib.next(), 5);
            assertEquals(fib.next(), 8);
        
    }
}
